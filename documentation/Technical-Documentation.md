The Income Approach to Valuation of Commercial Apartments
=========================================================

Due to the relatively lower number of sales of commercial apartments,
those properties are valued according to the [‘income
approach.’](https://en.wikipedia.org/wiki/Income_approach) Commercial
multi-family properties are income-producing assets, and their purchase
prices reflect the [time-discounted
value](https://en.wikipedia.org/wiki/Present_value) of the income
streams they produce. There are a number of similar approaches to
estimating the fair cash value of these properties. Each approach relies
on a basic valuation equation:
``` math
    \text{Fair Cash Value}_i=\vec{S_i} * E(\vec{M_j}) * (1-E(V_j)) * (1-E(E_j))* (1-E(F_j)) * \frac{1}{E(C_j)}
```
There are a number of key concepts in this equation it is important to
understand. The subscripts on the variables indicate whether the value
is specific to the property, i, or whether it is applied to some group
of properties, j, which contains the specific property i.

### Definitions

**Gross Potential Income (GPI)** is defined as the maximum amount of
income a property can generate over a set period of time. It is simply
the product of the spaces vector, $\\vec{S\_i}$, and their annual fair
rental values, as measured from the market, $\\vec{M\_j}$. For example,
if you have a property with 10 1-bedroom units and 10 2-bedroom units,
and those units are renting, on average, for $800 and $1,000 in that
propertie’s class and market, you have the simply matrix multiplication:
``` math
    \text{GPI_i} =  \vec{S_i} * \vec{M_j} * 12 =  \begin{bmatrix} 10 \\ 10 \end{bmatrix} * \begin{bmatrix} 800 & 1100\end{bmatrix}*12=1,092,000
```
It is important to note that the market rates applied to the spaces
vector are *expected* rents per unit, not actual rents per unit. This is
noted in the equation by the expectation function *E*(.) and the
subscript j instead of i. There are a number of reasons the actual
rental amounts in a building will differ from current market rents:
longer-than-average unit tenures, poor management, owner-occupied units,
garden apartments, family & friends discounts, etc. The fair cash value
of any given property is based on market rents in the property’s asset
class and region.

**Net Potential Income (NPI)** is GPI net of vacancy, expenses, and free
rent. Again, for each of these, we estimate their rates from the asset
class and region of any given property. For example, if the typical
apartment in a given asset class and region is vacant about one month
every year (1/12 ~ 0.083), and expenses for these sorts of properties
are about 45% of gross revenues, and properties in this class and region
typically fail to collect about 5% of gross rents, you have the
calculation:
``` math
    \text{NPI} = 1,092,000 * (1-.083)*(1-.45)(1-.05)=523,213
```
**Capitalization Rate (Cap rate)** is a function of the discount rate
applied to the expected annual net cash flow, as well as the expected
future growth or decline in NPI. A cap rate of 7.5% would yiel the
following calculation:

     \text{Fair Cash Value}= \text{NPI_i}*\frac{1}{C_j}=523,213*\frac{1}{.075}=6,976,169

Which is to say that a purchaser would pay $6,976,169 for a property in
this class and market with the given rentable spaces.

Calculating each component of the income approach
=================================================

There are two groups of commercial apartments that need to be
considered: properties that completed a [Real Property Income & Expense
Online Form
(RPIE)](https://rpie.cookcountyassessor.com/account/start-filing), and
properties that have not.

Spaces - RPIE Filers
--------------------

The RPIE application collects highly detailed data on individual spaces
within buildings. Filers first define buildings, and associate them with
PINs. They then define spaces in each building. The system assignes
unique identifiers to each building and space such that spaces have a
1:1 corrospondance with buildings. Fields that may be completed are:

<table>
<colgroup>
<col style="width: 47%" />
<col style="width: 52%" />
</colgroup>
<thead>
<tr class="header">
<th>Field name</th>
<th>Field values</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Unit</td>
<td>e.g. 101, 1G, 1st floor, etc.</td>
</tr>
<tr class="even">
<td>Months vacant during prior 12 months</td>
<td>Reported in two-month increments, e.g. 0, 0-2, 3-4, etc.</td>
</tr>
<tr class="odd">
<td>Vacant on date of submission?</td>
<td>Yes/No</td>
</tr>
<tr class="even">
<td>Affordable / Subsidized</td>
<td>No, Yes (Project Based), Yes LIHTC</td>
</tr>
<tr class="odd">
<td>Garden apartment</td>
<td>Yes/No</td>
</tr>
<tr class="even">
<td>Owner Occupied on date of submission?</td>
<td>Yes/No</td>
</tr>
<tr class="odd">
<td>Rooms</td>
<td>Number</td>
</tr>
<tr class="even">
<td>Bedrooms</td>
<td>Number</td>
</tr>
<tr class="odd">
<td>Bathrooms</td>
<td>Number</td>
</tr>
<tr class="even">
<td>Square feet</td>
<td>Number</td>
</tr>
<tr class="odd">
<td>Lease start date</td>
<td>Date</td>
</tr>
<tr class="even">
<td>Lease end date</td>
<td>Date</td>
</tr>
<tr class="odd">
<td>Base rent per month</td>
<td>$ amount</td>
</tr>
<tr class="even">
<td>Free or Uncollectable Rent</td>
<td>$ amount</td>
</tr>
</tbody>
</table>

In order to algorithmically value commercial apartments, we need to
overcome two challenges. First, we nee to know the vector of spaces for
each apartment building and/property. This will be achieved through
[Commercial Apartment Property Characteristics
Assembled](https://gitlab.com/groups/ccao-data-science---modeling/-/milestones/23).

Second, we need to be able to estimate each parameter of the first
equation above using data. This will be achieved through [Predict Space
Values](https://gitlab.com/groups/ccao-data-science---modeling/-/milestones/24).

Criteria for Success
====================

We have to be able to produce a full set of preliminary apartment values
by November for review by valuations. These values need to produce
reasonably accurate estimates.
