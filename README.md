# Commercial Apartment CAMA

The commercial apartment AVM is data science's attempt to automate the valuation of apartment buildings (classes 313, 314, 315, 318, 391, 396 - defined [here](https://gitlab.com/ccao-data-science---modeling/documentation/wiki_content/-/blob/master/Data/class-definitions.pdf)).

## Data
The modeling pipeline leverages data from a number of sources:

- Internal, historic CCAO valuation data stored in [excel sheets](G:\1st Pass spreadsheets) which contain commercial valuation analysts' work on determining the number of units in a building, rent, the number of bedrooms, bathrooms, and square footage associated with those units

- RPIE, the Real Property Income & Expense online form used to collect detailed commercial and residential unit-level data on income-producing parcels throughout Cook County, stored on an internal SQL mirror of Clarity Partners, LLC database

- Data science's SQL server, which includes
  - A mirror of the AS400, the internal database of record for the CCAO which stores essential hisotircal valuation information from both the Assessor's office and the Board of Review by PIN
  - Useful fact tables that allow data science to quickly merge PINs with geographic and governmental attributes either manually or through pre-constructed views
<br/><br/>
- TrueRoll, a partner of the CCAO that provides apartment unit level characteristics and rent scraped from online listings

- Trepp and CoStar, data analytic services in the commercial real estate sector

- The Census Bureau's American Community and Pulse Surveys

- The [Institute for Housing Studies](https://www.housingstudies.org/) at DePaul University which graciously provides a housing price index to the public

Using characteristics collected from RPIE, supplemented with Tyler data and joined to tract-level census covariates as well as time-adjusted median single family and condo sale prices, the AVM constructs a data set on which to train a model for predicting *apartment unit-level* rent.

## Building Fair Market Values
The model is then used to predict rents across all the units gathered from the internal, historic CCAO valuation data, which we consider our universe of apartment units (it must be noted that this data is not complete or consistent).  Rents then undergo a number of transformations in order to arrive at a PIN-level Fair Market Value (FMV):

- Mean rents are annualized and summed by PIN to calculate GPI (gross potential income)

- Using census data nonpayment rates are built across income brackets and by tracts

- Tract-level vacancy from the census is used  either directly, or to build a model for predicting vacancy using other socioeconomic indicators
  - Whichever method produces vacancy rates that correlate better with those observed in RPIE data is chosen
<br/><br/>
- Financial data submitted to RPIE is scraped for usable text and anonymized using AWS

- The pipeline uses this data to build expense ratios (across 6 dimensions, an indicator for a building being older than 75 years, which we use as a proxy for steam heating, and the three county triads - North, South, Chicago) where it's available, supplementing it with expense ratios from Trepp
  - Applying these expense ratios, along with vacancy and nonpayment rate to GPI, the pipeline calculates NPI (net potential income)
<br/><br/>
- Cap rates from Trepp are aggregated across class and municipality (or larger geographies when data is too sparse), joined to the GPI data, and FMV is calculated

## Pipeline
#### 0 STAGE

1. ingest historical values from across past three years (all the tris) to create a complete sample of PINs that contain commercial apartments (based on CLASS)
 - this is temporary, until the RPIE database is robust enough supplant this data - we believe this data to be highly suspect, and handling it is diffuicult due to its disorganized and insconssitent nature
 - unit characteristics (SQFT/rooms) are usually a function of average building square footage divided by the number of units
 - we hope that using this data as a baseline for commercial apartments will motivate tax payers to provide us with more accurate data
 - not all commectial apartment class PINs can be found in this data. those that are missing do not have any unit-level observations
 - we combine this data with tract-level census data

2. a training data set is compiled using RPIE data, combined with various census data
 - this data is reported by taxpayers who wish to file a commercial appeal
 - kept on the RPIE SQL server as "residential" spaces

3. data supplied by TrueRoll can be appended onto RPIE data to increase the sample size for trainining the model given its current size limitation

4. financial data collected through RPIE is scrubbed of personal information and cleaned to build expense ratios

5. data collected from Trepp and Costar are cleaned and prepared to build cap rates

#### 1 STAGE

- outputs summary stats on RPIE field completeness

#### 2 STAGE

- runs regression using RPIE (and TrueRoll, if indicated) data
- assigns median for missing covariates within tract/township if desired
- replaces valuations historic excel sheet data with RPIE data where available, if desired
- predicts rents on valuation department's data
- collapses rent at building (PIN since IC doesn't track improvements) level

#### 3 STAGE

- using census rental non-payment data and assumptions about how often each income group misses payments adjust GPI per building

#### 4 STAGE

- trains vacancy model using tract level census vacancy and demographic data
- predicts vancancy rates for buildings
- compares whether these predictions are more accurate based on RPIE data than baseline tract level vacancy data, chooses whichever is more accurate
- updates GPI

#### 5 STAGE

- using expense data extratced through AWS textract in 0.4 median expense ratios across an old age indicator, building size, and triad
- when RPIE data is not available, Trepp implied expense ratios are used
- updates GPI

#### 6 STAGE

- cap rates are calculated across class and municipality using historic Trepp data

#### 7 STAGE

- GPI for 318s is adjusted
- 318s have commercial spaces on their first floor and we use median commercial rent per sqft for 318s * lot size to estimate a value for the first floor of 318s
- all FMVs within class and triad are adjusted so that median ratios are 1

#### 8 STAGE

- exports data for desk review and for upload to socrata
- scorata data comes in 2 forms - unit level and PIN level

#### REPORTING

- creates a number of plots/map/tables for report
