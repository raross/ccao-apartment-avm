# this script calculates the percentage of missed rental payments in a census tract by multiplying
# the probability of missing a payment conditional on income group times the number of payments missed in the year
# the first probability comes from the census, the second from our own assumption

# to estimate the rental non-payment rate in each census tract, we have
# to form assumptions about how many months someone will miss in a year based on income.
# CURRENTLY WE ASSUME ALL INCOME GROUPS MISS THE SAME NUMBER OF MONTHS
rental_nonpayment <- rental_nonpayment %>%
  mutate(percent_missed = rental_npr * c(2, 2, 2, 2, 2, 2, 2, 2) / 12)

# for integrity check
gpi_rows_ante <- nrow(gpi)

# join non-payment rates to gpi
gpi <- gpi %>% left_join(

  # calculate non-payment rate per tract, given income distribution
  census_incomes %>%

    # multiply each column in census incomes with the appropriate percent missed figure in rental_nonpayment
    mutate(predicted_non_payment_rate = rowSums(t(t(census_incomes %>% select(rental_nonpayment$hh_income)) *
                                                    rental_nonpayment$percent_missed)) / total_reporting_income,
           census_tract = str_pad(census_tract, 6, side = "left", pad = "0")) %>%

    # Eliminate NAs by setting them to the minimum non-payment rate
    mutate(predicted_non_payment_rate = tidyr::replace_na(predicted_non_payment_rate,
                                                   min(.$predicted_non_payment_rate, na.rm = TRUE))) %>%

    # remove unwanted columns
    select(census_tract, predicted_non_payment_rate)

  )

# INTEGRITY CHECKS ----

integrity_checks <- c()
integrity_checks[1] <- "*** INTEGRITY CHECKS ***"

# check if gpi/census join went as planned
integrity_checks[2] <- paste0(gpi_rows_ante - nrow(gpi),
                              " observations added joining census data to gpi (0 expected)")

# check if gpi is unique by key_pin
integrity_checks[3] <- paste0(nrow(gpi %>% dplyr::filter(duplicated(key_pin))),
                              " duplicate key_pin(s) in gpi (0 expected)")

writeLines(integrity_checks)
rm(integrity_checks)