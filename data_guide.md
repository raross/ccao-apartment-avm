# Commercial Apartment CAMA Data Objects

There are two primary r file types created and leveraged in the apartment CAMA as it currently exists, 0_data.rda and reporting_data.rda.  0_data is all of the data needed to initialize the modeling portion of the pipeline.  Reporting data is the product of the modeling portion of the pipeline and includes all the data needed for the valuation report.  The contents of each. rda file are outlined below.  The script in which an object is intitially constructed follows the object names.

## Common data sources

***RPIE***  
Data collected through the [Real Property Income & Expense](https://rpie.cookcountyassessor.com/) web portal.  Taxpayers file RPIEs in order to appeal commercial property tax bills.  This data is not currently public-facing and resides on a CCAO SQL server.

***Valuations' Excel Sheets***  
Parcel level data collected and maintained by analysts in the Industrial/Commercial Valuations group.  As parcels are assessed on a three year rotation according to triad, observations for a given parcel will only be available in one of the three last years.  Structe of sheets is inconsistent across classes, years, and analysts for these sheets.  Sheets are stored on a shared CCAO network drive.

***US Census***  
Data retrieved primarily from the [National Historical Geographic Information System](https://www.nhgis.org/).

***Trepp***  
Third-party commercial sales and appraisal data.

***TrueRoll***  
Third-party partner that provides apartment listing information from various sources across the internet.

***AS400***  
Cook County's system of record.  Parcel-level data accessed through data science's SQL server mirror.

## Objects present in both .rda files

***all_apartment_PINs*** `0b_ingest_sql_and_census_data`  
All class 313, 314, 315, 318, 391, 396 parcels from 2017-2020.  ***NOT*** unique by parcel and year because it includes sales.  If a parcel was sold more than once in a given year it will appear more than once within that year.  Contains only AS400 sourced data other than the `median_commercial_rentperSQFT` field, which comes from RPIE data aggregated at townships and triads.

***ccao_apartments*** `0c_ingest_ic_excel_sheets`  
A set of county-wide apartment units constructed using valuation analysts' excel sheets.  Data comes from across the last three triennials in order to compose a county-wide data set.  It begins as parcel level data and is expanded based on the number and type of apartments within a given PIN.  All apartment class (313, 314, 315, 318, 391, 396) parcels are contained with the data set, regardless of whether they were found within the valuations` excel sheets or not.  Parcels not found in the excel sheets contain no apartment charactersitics data.

***limitations*** `0c_ingest_ic_excel_sheets`  
List containing shortcomings in our data, including the number of tracts without either single-family or condo sales data since 2013, and apartment class PINs not found in either the "key pin" or "related pins" fields in valuations excel sheets.

***modeling_data*** `0b_ingest_sql_and_census_data`  
Apartment unit-level data collected from RPIE and potentially combined with third-party data from partners such as TrueRoll.  Only contains observations from those data sources, is not unique by PIN.

***params*** `main`  
A list of pipeline parameters chosen by whomever most recently ran it from start to finish.

***rental_nonpayment*** `0b_ingest_sql_and_census_data`  
Census data selected from "Last Month`s Payment Status for Owner-Occupied Housing Units, by Select Characteristics: Illinois" detailing rental non-payment rates by income bracket.

***trepp*** `0d_ingest_trepp_costar`  
Data retieved from Trepp and re-assmbled.  Data is a set of sales and financial data associated with those sales.  Primarily used for cap rates and to compare appraisals to AVM produced FMVs.

## 0_data

***affordable_pins*** `0b_ingest_sql_and_census_data`  
A list of parcels associated with affordable housing units in Chicago provided by the city of Chicago.

***census_data***  `0b_ingest_sql_and_census_data` 
Tract-level data for Cook County, containing information on rent, and socioeconomic measures sourced from the US census.

***census_incomes*** `0b_ingest_sql_and_census_data`  
Tract-level income data for Cook County sourced from the US census.

***exemption_project*** `0a_ingest_third_party_apt_data`  
Apartment listing data provided by TrueRoll.  Contains non-apartment class parcels, classified as "condo" or "other".  Data is not complete at a parcel level, each observation is for a given apartment listing found online.

***expenses_8825*** `0f_ingest_8825s`  
Expense and income data collected through RPIE.  Can be joined by PIN.  From tax form 8825`s, "Rental Real Estate Income and Expenses of a Partnership or an S Corporation"

***expenses_sch_e*** `0e_ingest_schedule_es`  
Expense and income data collected through RPIE.  Can be joined by PIN.  From Schedule E`s (Form 1040 or 1040-SR), "Supplemental Income and Loss"

***replacement*** `0c_ingest_ic_excel_sheets`  
RPIE apartment unit observations that can be used to replace apartment unit observations in `ccao_apartments`.  Data would be replaced since RPIE data is believed to be more accurate than valuations` excel sheets.

***single_family_fmv*** `0b_ingest_sql_and_census_data`  
Tract-level median single-family and condo sale prices using DePaul Hedonic Price Index adjusted sales data since 2013.  Tracts that have no sales data use CCAO neighborhood or township-level median sale prices when necessary.

***vacancy*** `0b_ingest_sql_and_census_data`  
Tract-level census housing vacancy data.

## reporting_data

***apartment_building*** `2_predict_rents`  
`modeling_data` collapsed by BuildingId, with mean units characteristics, predicted rents, and GPI, as well as an indicator for the presence of an affordable building associated with the PIN associated with the BuildingId included.  

***expense_ratios*** `5_construct_expense_ratios`  
Data constructed using both RPIE tax expense forms and expense ratios calculated by Trepp. RPIE data recieves precedence when a PIN has an expense ratio from both sources.  This data is used to create median expense ratios by  building size (number of units in a building) terciles (small, medium, large) which are unique to triads, and an indicator for a building being "old", which is defined as 75 years or older.

***filtered*** `7_calculate_fmv_adjust_318s`  
Data set containing sales with AVM and non-AVM sales-ratios greater than 2, source of ratio is noted in `source` column.  Not expected to be unique by PIN or sale. Direct descendant of `ratios_ds` and `ratios_ic`.

***gpi*** `2_predict_rents`  
The primary PIN level valuation object.  `ccao_apartments` is used as a universe of apartment units on which to predict unit rents, then collapsed by PIN.  Values from several other objects are joined on by PIN, tract, or another value in order to construct FMVs for each PIN with enough data to have recieved predicted rents.

***ratios_ds*** `7_calculate_fmv_adjust_318s`  
Sales from 2019 and later, as well as AVM produced FMVs and sales ratios.  Not all sales are valid, but a valid indicator is included.

***ratios_ic*** `7_calculate_fmv_adjust_318s`  
Sales from 2017 and later, as well as non-AVM produced FMVs and sales ratios.  Not all sales are valid, but a valid indicator is included.  

***rent_model*** `3_predict_rental_nonpayment`  
The model trained on RPIE (and possibly supplemental 3rd party data) and used to predict rents for all observations in `ccao_apartments` with enough charactersitics data.

***replaced*** `2_predict_rents`  
Observations from `ccao_apartments` that have been replace with RPIE data.

***rpie_vacancy*** `4_predict_vacancy`  
Contains tract-level vacancy data constructed using RPIE data, census data, and vacancy predicted by a model trained on tract-level census data.

***vacancy_method*** `4_predict_vacancy`  
An object that indicates which vacancy data the pipeline used to construct NPI.  It will choose either census vacancy data or data generated by the vacancy model depending on which is better correlated with RPIE vaccancy data.

***vacancy_model*** `4_predict_vacancy`  
A model used to predict vacancy rates at a tract-level. Trained on census vacancy and socioeconomic data.  Output can be used to construct NPI if it is more correlated with RPIE vaccancy data than census provided vacancy data is.
